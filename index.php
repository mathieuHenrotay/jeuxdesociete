<?php get_header(); ?>
<?php
	$paged1 = isset($_GET['paged1']) ? (int) $_GET['paged1'] : 1;
?>
<main>
	<!--*********************************************SECTION DERNIERS JEUX*******************************************-->
	      <div class="boite-fond-coul2-claire">
	        <div class="boite-fixe">
						<!--<div class="ancre" id="ancre1"></div>-->
	          <section id="DerniersJeux" class="grille-g section">
	              <h2 class="titre">Tous nos jeux</h2>
								<?php
									$sticky = get_option( 'sticky_posts' );
									$args = array(
										'posts_per_page' => 9,
										'ignore_sticky_posts' => 1,
										'orderby' => 'title',
										'order' => 'ASC',
										'paged' => $paged1
									);
									$query_tousLesJeux=new WP_query($args);
								?>
								<?php if($query_tousLesJeux->have_posts()) : ?>
								<?php while($query_tousLesJeux->have_posts()) : $query_tousLesJeux->the_post(); ?>
									<article class="m6 l4 cards cards-type-m hover-image-grow-rotate">
											<a href="<?php the_permalink(); ?>#detailsJeu">
												<figure class="height-s parent-image-grow-rotate">
													<img src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php echo get_the_title(get_post_thumbnail_id()); ?>" class="image-grow-rotate">
													<figcaption class="xs-cache">
														<?php echo get_the_title(get_post_thumbnail_id()); ?>
													</figcaption>
												</figure>
											</a>
											<div class="cards-texte">
												<?php the_category(); ?>
												<a href="<?php the_permalink(); ?>#detailsJeu"><h3 class="contenu-centre"><?php the_title(); ?></h3></a>
												<div class="contenu-centre">
													<?php the_tags('<div class="post-tags"><i class="fas fa-user-tie"></i> ','','</div>'); ?>
												</div>
												<p><?php echo substr(get_the_content(), 0, strrpos(substr(get_the_content(), 0, 250), ' ')).'...' ?></p>
												<a href="<?php the_permalink() ?>#detailsJeu" class="btn btn-fond-coul1 call-to-action contenu-centre">Plus d'infos</a>
											</div>
									</article>
								<?php endwhile; ?>
								<?php endif; ?>
								<?php wp_reset_postdata(); ?>
								<div class="pagination">
									<?php $paginate1_args = array(
										'format'             => '?paged1=%#%#DerniersJeux',
										'total'              => $query_tousLesJeux->max_num_pages,
										'current'            => $paged1,
										);
									?>
									<?php echo paginate_links($paginate1_args);?>
								</div>
	          </section>
	        </div>
	      </div>
<!--***********************************************FIN DERNIERS JEUX************************************************************-->

</main>
<?php get_footer(); ?>
