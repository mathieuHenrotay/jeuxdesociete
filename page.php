<?php get_header(); ?>
<main>
	<!--********************************************SECTION DES pictos*********************************************-->
	    <div class="boite-fond-coul2">
	        <div class="boite-fixe">
	          <section id="pictos" class="section">
	            <h2 id="test" class="titre effet-apparition apparition-top">Nos services</h2>
	            <ul class="grille-g liste-no-puce contenu-centre">
	              <li class="m6 l3 effet-apparition apparition-bottom" data-delay="500">
	                <i class="fas fa-thumbs-up contenu-XXGrand"></i>
	                <h3>Jeux de qualité</h3>
	                <p class="boite-marge-nulle">Nous sélectionnons pour vous les jeux les plus originaux et qualitatifs du marché. Avec nous, vous en aurez pour votre argent !</p>
	              </li>
	              <li class="m6 l3 effet-apparition apparition-bottom" data-delay="500">
	                <i class="fas fa-clipboard-check contenu-XXGrand"></i>
	                <h3>Testés avec amour</h3>
	                <p class="boite-marge-nulle">Chaque jeu a été testé scrupuleusement par notre équipe sur une quinzaine de critères. Vous êtes exigeant, nous aussi !</p>
	              </li>
	              <li class="m6 l3 effet-apparition apparition-bottom" data-delay="500">
	                <i class="fas fa-truck contenu-XXGrand"></i>
	                <h3>Livrés dans la journée</h3>
	                <p class="boite-marge-nulle">Quand on achète quelque chose on aime le recevoir rapidement ! Chez jeuxdesociété.com, nous vous livrons le jour même si vous commandez avant 12h.</p>
	              </li>
	              <li class="m6 l3 effet-apparition apparition-bottom" data-delay="500">
	                <i class="fas fa-hand-holding-usd contenu-XXGrand"></i>
	                <h3>Satisfait ou remboursé</h3>
	                <p class="boite-marge-nulle">Nous sommes tellement convaincu de nos jeux que nous vous offrons 30 jours de retour gratuit sans justificatif. Qui peut en dire autant ?</p>
	              </li>
	            </ul>
	          </section>
	        </div>
	      </div>
	<!--***********************************************FIN PICTOS************************************************************-->

	<!--***********************************************SECTION COUP DE COEUR**************************************************-->
	        <section id="coupDeCoeur" class="grille-no-g section-top">
	            <h2 class="titre effet-apparition apparition-top">Nos coups de coeur</h2>
							<?php
								$sticky = get_option( 'sticky_posts' );
								$args = array(
									'posts_per_page' => 4,
									'post__in'=> $sticky,
									'orderby' => 'title',
									'order'=>'ASC'
								);
								$query_favoris=new WP_query($args);
							?>
							<?php if($query_favoris->have_posts()) : ?>
							<?php while($query_favoris->have_posts()) : $query_favoris->the_post(); ?>
		            <article class="l6 cards cards-type-l hover-blur-greyscale hover-image-grow effet-apparition">
		                <a href="<?php the_permalink(); ?>#detailsJeu">
		                  <figure class="height-l parent-image-grow">
		                    <img src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php echo get_the_title(get_post_thumbnail_id()); ?>" class="image-blur-greyscale image-grow">
		                    <figcaption class="xs-cache">
		                      <?php echo get_the_title(get_post_thumbnail_id()); ?>
		                    </figcaption>
		                  </figure>
		                </a>
		                <div class="cards-texte">
												<?php the_category();?>
		                  <a href="<?php the_permalink(); ?>#detailsJeu"><h3 class="contenu-centre"><?php the_title(); ?></h3></a>
		                  <div class="contenu-centre">
												<?php the_tags('<div class="post-tags"><i class="fas fa-user-tie"></i> ','','</div>'); ?>
		                  </div>
		                </div>
		            </article>
							<?php endwhile; ?>
							<?php endif; ?>
							<?php wp_reset_postdata(); ?>
	        </section>
	<!--***********************************************FIN COUP DE COEUR***********************************************************-->

	<!--*********************************************SECTION DERNIERS JEUX*******************************************-->
	      <div class="boite-fond-coul2-claire">
	        <div class="boite-fixe">
	          <section id="DerniersJeux" class="grille-g section ancre">
	              <h2 class="titre effet-apparition apparition-top">Jeux du moment</h2>
								<?php
									$sticky = get_option( 'sticky_posts' );
									$args = array(
										'posts_per_page' => 6,
										'post__not_in'=> $sticky,
										'orderby' => 'title',
										'order'=>'ASC'
									);
									$query_derniersJeux=new WP_query($args);
								?>
								<?php if($query_derniersJeux->have_posts()) : ?>
								<?php while($query_derniersJeux->have_posts()) : $query_derniersJeux->the_post(); ?>
		              <article class="m6 l4 cards cards-type-m hover-image-grow-rotate effet-apparition apparition-scale">
		                  <a href="<?php the_permalink(); ?>#detailsJeu">
		                    <figure class="height-s parent-image-grow-rotate">
		                      <img src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php echo get_the_title(get_post_thumbnail_id()); ?>" class="image-grow-rotate">
		                      <figcaption class="xs-cache">
														<?php echo get_the_title(get_post_thumbnail_id()); ?>
		                      </figcaption>
		                    </figure>
		                  </a>
		                  <div class="cards-texte">
												<?php the_category(); ?>
		                    <a href="<?php the_permalink(); ?>#detailsJeu"><h3 class="contenu-centre"><?php the_title(); ?></h3></a>
		                    <div class="contenu-centre">
													<?php the_tags('<div class="post-tags"><i class="fas fa-user-tie"></i> ','','</div>'); ?>
		                    </div>
		                    <p><?php echo substr(get_the_content(), 0, strrpos(substr(get_the_content(), 0, 250), ' ')).'...' ?></p>
		                    <a href="<?php the_permalink() ?>#detailsJeu" class="btn btn-fond-coul1 call-to-action contenu-centre">Plus d'infos</a>
		                  </div>
		              </article>
								<?php endwhile; ?>
								<?php endif; ?>
								<?php wp_reset_postdata(); ?>

	          </section>
	        </div>
	      </div>
<!--***********************************************FIN DERNIERS JEUX************************************************************-->
<!--***********************************************SECTION TOUS LES JEUX*****************************************************-->
			<?php
				$args = array(
					'posts_per_page' => 1,
					'ignore_sticky_posts' => 1,
					'orderby' => 'rand'
				);
				$query_background=new WP_query($args);
			 ?>
			<?php if($query_background->have_posts()) : ?>
			<?php while($query_background->have_posts()) : $query_background->the_post(); ?>
			<?php
				$backgroundTousLesJeux = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
			?>
			<section id="TousLesJeux" class="section" style="background: url('<?php echo $backgroundTousLesJeux[0]; ?>') center no-repeat; background-size:cover;background-attachment: fixed;">

          <div class="contenu-centre boite-fixe">
            <h2 class="titre">Vous en voulez encore ?</h2>
            <a href="<?php echo get_post_type_archive_link('post'); ?>#DerniersJeux" class="btn btn-fond-coul1 call-to-action">Voir tous les jeux</a>
          </div>

      </section>
			<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
<!--***********************************************FIN TOUS LES JEUX************************************************************-->
		<section class="section boite-fond-coul2-claire" id="contact">
			<h2 class="titre effet-apparition">Contactez-nous</h2>
			<div class="boite-fixe">
					<div class="effet-apparition apparition-bottom">
						<iframe width="100%" height="396px" src="https://maps.google.com/maps?width=100%&amp;height=300&amp;hl=en&amp;q=Houte-Si-Plou+(JeuxDeSoci%C3%A9t%C3%A9.com)&amp;ie=UTF8&amp;t=&amp;z=14&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/map-my-route/">Plot a route map</a></iframe>
					</div>

					<div class="formulaireDeContact effet-apparition apparition-top">
						<h3 class="contenu-centre contenu-majuscule contenu-graisse-normal boite-marge-nulle">On peut vous aider?</h3>
						<?php echo do_shortcode('[caldera_form id="CF5c237b9f03a3b"]'); ?>
					</div>
			</div>
		</section>
</main>
<?php get_footer(); ?>
