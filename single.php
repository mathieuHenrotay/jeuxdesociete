<?php get_header(); ?>
<main>
	<!--*********************************************SECTION DETAILS JEUX*******************************************-->
				<?php $id=$post->ID; ?>
				<?php
					$categories=get_the_category($post->ID);
					for($i=0;$i<count($categories);$i++):
						if($i>0):
							$catString.='/'.$categories[intval($i)]->cat_ID;
						else:
							$catString.=$categories[intval($i)]->cat_ID;
						endif;
					endfor;
					$catTabs=explode('/', $catString);
				 ?>
	            <article id="detailsJeu" class="section">
								<div class="boite-fixe">
	                  <h2 class="titre"><?php the_title(); ?></h2>
	                  <figure class="contenu-centre image-responsive-max">
	                    <?php the_post_thumbnail([800]); ?>
	                    <figcaption class="xs-cache">
												<?php echo get_the_title(get_post_thumbnail_id()); ?>
	                    </figcaption>
	                  </figure>

										<?php the_category(); ?>

										<div class="contenu-centre">
											<?php the_tags('<i class="fas fa-user-tie"></i> '); ?>
										</div>
	                  <div class="texteJeu">
											<?php the_content(); ?>
	                  </div>
								</div>
	                  <ul class="liste-no-puce grille-g nextPreviousNav">
											<?php
												$next_post = get_next_post();
												if (!empty( $next_post )): ?>
												 <li class="m6 contenu-centre">
													<a href="<?php echo bloginfo('url').'/'.$next_post->post_name.'#detailsJeu'?>">
														<figure class="height-m image-cover">
 													 		<img src="<?php echo get_the_post_thumbnail_url($next_post) ?>" alt="<?php echo get_the_title(get_post_thumbnail_id($next_post->ID)); ?>">
 													 	</figure>
														<div class="btn btn-fond-coul1">
															<div class="faIcon">
																<i class="fas fa-angle-left contenu-XXGrand"></i>
															</div>
															<div>
																<?php echo $next_post->post_title; ?>
															</div>
														</div>
													</a>
												 </li>
											 <?php endif; ?>
											<?php
												$prev_post = get_previous_post();
												if (!empty( $prev_post )): ?>
												<li class="m6 contenu-centre">
												 <a href="<?php echo bloginfo('url').'/'.$prev_post->post_name.'#detailsJeu'?>">
													 <figure class="height-m image-cover">
														 <img src="<?php echo get_the_post_thumbnail_url($prev_post) ?>" alt="<?php echo get_the_title(get_post_thumbnail_id($prev_post->ID)); ?>">
													 </figure>
													 <div class="btn btn-fond-coul1">
														 <div>
															 <?php echo $prev_post->post_title; ?>
														 </div>
														 <div class="faIcon">
															 <i class="fas fa-angle-right contenu-XXGrand"></i>
														 </div>
													 </div>
												 </a>
												</li>
											 <?php endif; ?>
	                  </ul>
	            </article>

	<!--***********************************************FIN DETAILS JEUX************************************************************-->
	<!--*********************************************SECTION DERNIERS JEUX*******************************************-->
				<?php
					$args = array(
						'posts_per_page' => 6,
						'post__not_in'=>[$id],
						'category__in'=>$catTabs,
						'ignore_sticky_posts' => 1,
						'orderby' => 'rand'
					);
					$query_derniersJeux=new WP_query($args);
				?>
				<?php if($query_derniersJeux->have_posts()) : ?>
					<div class="boite-fond-coul2-claire">
						<div class="boite-fixe">
							<section id="DerniersJeux" class="grille-g section">
									<h2 class="titre">Vous aimerez également</h2>
									<?php while($query_derniersJeux->have_posts()) : $query_derniersJeux->the_post(); ?>
										<article class="m6 l4 cards cards-type-m hover-image-grow-rotate">
												<a href="<?php the_permalink(); ?>#detailsJeu">
													<figure class="height-s parent-image-grow-rotate">
														<img src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php echo get_the_title(get_post_thumbnail_id()); ?>" class="image-grow-rotate">
														<figcaption class="xs-cache">
															<?php echo get_the_title(get_post_thumbnail_id()); ?>
														</figcaption>
													</figure>
												</a>
												<div class="cards-texte">
													<?php the_category(); ?>
													<a href="<?php the_permalink(); ?>#detailsJeu"><h3 class="contenu-centre"><?php the_title(); ?></h3></a>
													<div class="contenu-centre">
														<?php the_tags('<div class="post-tags"><i class="fas fa-user-tie"></i> ','','</div>'); ?>
													</div>
													<p><?php echo substr(get_the_content(), 0, strrpos(substr(get_the_content(), 0, 250), ' ')).'...' ?></p>
													<a href="<?php the_permalink() ?>#detailsJeu" class="btn btn-fond-coul1 call-to-action contenu-centre">Plus d'infos</a>
												</div>
										</article>
									<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>

						</section>
					</div>
				</div>
<!--***********************************************FIN DERNIERS JEUX************************************************************-->

<!--***********************************************SECTION TOUS LES JEUX*****************************************************-->
			<?php
				$args = array(
					'posts_per_page' => 1,
					'ignore_sticky_posts' => 1,
					'orderby' => 'rand'
				);
				$query_background=new WP_query($args);
			 ?>
			<?php if($query_background->have_posts()) : ?>
			<?php while($query_background->have_posts()) : $query_background->the_post(); ?>
			<?php
				$backgroundTousLesJeux = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
			?>
			<section id="TousLesJeux" class="section" style="background: url('<?php echo $backgroundTousLesJeux[0]; ?>') center no-repeat; background-size:cover;background-attachment: fixed;">

          <div class="contenu-centre boite-fixe">
            <h2 class="titre">Vous en voulez encore ?</h2>
            <a href="<?php echo get_post_type_archive_link('post'); ?>" class="btn btn-fond-coul1 call-to-action">Voir tous les jeux</a>
          </div>

      </section>
			<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
<!--***********************************************FIN TOUS LES JEUX************************************************************-->


</main>
<?php get_footer(); ?>
