<?php

	if ( post_password_required() ) {
		return;
	}
?>

<div id="comments">

	<?php if ( have_comments() ) : ?>
		<h2>
			<?php
				printf( _x( 'Un commentaire sur &ldquo;%s&rdquo;', 'comments title', 'videotheque' ), get_the_title() );
			?>
		</h2>

		<ol>
			<?php
				wp_list_comments( array(
					'style'       => 'ol',
					'short_ping'  => true,
					'avatar_size' => 56,
				) );
			?>
		</ol>

	<?php endif; ?>

	<?php
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p><?php _e( 'Commentaires sont clos.', 'videotheque' ); ?></p>
	<?php endif; ?>

	<?php comment_form(); ?>

</div>
