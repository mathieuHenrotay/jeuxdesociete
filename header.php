<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-with,  initial-scale=1">
		<title><?php the_title(); ?></title>
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
		<?php wp_head(); ?>
	</head>

	<body>
		<header id="entetePrincipale">
			<div id="barreNav" >
				<div class="boite-fixe">
					<div class="boite-marge-nulle boite-flottante-gauche">
						<a href="<?php bloginfo('url'); ?>">
							<h1 class="xs-cache">Jeux de société</h1>
							<img id="logo" src="<?php bloginfo('template_directory'); ?>/img/logo2.png" alt="Logo de jeux de jeuxdesociete.com">
						</a>
					</div>
					<!--NAVIGATION TABLETTES ET PC-->
					<nav class="m-visible menu menu-horizontal contenu-droite">
						<ul>
							<li><a href="<?php bloginfo('url'); ?>">Accueil</a></li>
							<li><a href="<?php echo get_post_type_archive_link('post'); ?>#DerniersJeux">Tous les jeux</a></li>
							<li class="dropDownMenu"><a href="<?php bloginfo('url'); ?>/categories">Catégories <i class="fas fa-caret-down"></i></a>
								<ul class="sousMenu xs-cache">
										<?php
											$catObjects=get_categories();
											foreach($catObjects as $catObject):
												$catID=$catObject->cat_ID;
												$catTitle=$catObject->name;
												$catLink=get_category_link( $catID );
												$catsTous.='<li><a href="'.$catLink.'#DerniersJeux" class="btn-fond-coul1">'.$catTitle.'</a></li>';
											endforeach;
											echo $catsTous;
										?>
								</ul>
							</li>
							<li class="dropDownMenu"><a href="<?php bloginfo('url'); ?>/editeurs">Editeurs <i class="fas fa-caret-down"></i></a>
								<ul class="sousMenu xs-cache">
									<?php
										$tagObjects=get_tags();
										foreach($tagObjects as $tagObject):
											$tagID=$tagObject->term_taxonomy_id;
											$tagTitle=$tagObject->name;
											$tagLink=get_category_link( $tagID );
											$tagsTous.= '<li><a href="'.$tagLink.'#DerniersJeux" class="btn-fond-coul1">'.$tagTitle.'</a></li>';
										endforeach;
										echo $tagsTous;
									?>
								</ul>
							</li>
							<li><a href="<?php bloginfo('url'); ?>/#contact">Contact</a></li>
						</ul>
					</nav>
					<!--NAVIGATION MOBILE -->
					<nav class="m-cache hamburger menu">
						<a href="#" id="hamburgerIcon">
							<div>
								<div></div>
								<div></div>
								<div></div>
							</div>
						</a>
						<div class="boite-clearfix"></div>
						<ul class="liste-no-puce contenu-centre xs-cache" id="navMobile">
							<li><a href="<?php bloginfo('url'); ?>" class="btn-fond-coul1">Accueil</a></li>
							<li><a href="<?php echo get_post_type_archive_link('post'); ?>" class="btn-fond-coul1">Tous les jeux</a></li>
							<li class="dropDownMenu"><a href="<?php bloginfo('url'); ?>/categories" class="btn-fond-coul1">Catégories <i class="fas fa-caret-down"></i></a>
									<ul class="sousMenu xs-cache liste-no-puce">
											<?php
												echo $catsTous;
											?>
									</ul>
							</li>
							<li class="dropDownMenu"><a href="<?php bloginfo('url'); ?>/editeurs" class="btn-fond-coul1">Editeurs <i class="fas fa-caret-down"></i></a>
								<ul class="sousMenu xs-cache liste-no-puce">
									<?php
										echo $tagsTous;
									?>
								</ul>
							</li>
							<li><a href="<?php bloginfo('url'); ?>/#contact" class="btn-fond-coul1">Contact</a></li>
						</ul>
					</nav>

				</div>
			</div>

			<article id="slider" class="hover-image-grow">

					<?php
						$args = array(
							'posts_per_page' => 1,
							'ignore_sticky_posts' => 1,
							'orderby' => 'rand'
						);
						$query_header=new WP_query($args);
					 ?>
					<?php if($query_header->have_posts()) : ?>
					<?php while($query_header->have_posts()) : $query_header->the_post(); ?>
						<a href="<?php the_permalink(); ?>#detailsJeu">
							<figure class="image-cover parent-image-grow">
								<img src="<?php the_post_thumbnail_url('full') ?>" alt="<?php echo get_the_title(get_post_thumbnail_id()); ?>" class="image-grow">
								<figcaption class="xs-cache">10-minutes to kill</figcaption>
							</figure>

							<div id="sliderContenu" class="contenu-centre">
								<h2 class="titre"><?php the_title(); ?></h2>
								<p class="s-visible"><?php echo substr(get_the_content(), 0, strrpos(substr(get_the_content(), 0, 300), ' ')).'...' ?></p>
								<!--J'ai mis un div car l'entièreté de l'élément est déjà clicable, si je remet un a ici ça bug -->
								<div class="btn btn-fond-coul1 call-to-action">Voir le jeu</div>
							</div>
						</a>
					<?php endwhile; ?>
					<?php endif; ?>
					<?php wp_reset_postdata(); ?>
			</article>

		</header>
	<!--***********************************************FIN NAV + SLIDER******************************************************-->
		<?php wp_reset_postdata(); ?>
