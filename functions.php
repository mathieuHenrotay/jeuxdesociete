<?php
// 	Support des images à la une
	add_theme_support('post-thumbnails');
// 	Support des menus de navigation
	add_theme_support('menus');
// 	Ajout de feuilles de style
	function add_new_styles() {
		wp_enqueue_style('fonts', 'https://fonts.googleapis.com/css?family=Raleway|Source+Sans+Pro');
		wp_enqueue_style('frameworks', get_stylesheet_directory_uri().'/css/frameworks.min.css');
		wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.5.0/css/all.css');
		wp_enqueue_style('app', get_stylesheet_directory_uri().'/css/app.min.css');
		wp_enqueue_script( 'jquery_ui', get_stylesheet_directory_uri() . '/js/jquery-ui.min.js', array( 'jquery' ), '1.0', true);
		wp_enqueue_script( 'css-transform', get_stylesheet_directory_uri() . '/js/jquery-css-transform.min.js', array( 'jquery' ), '1.0', true);
		wp_enqueue_script( 'animate-css-rotate-scale', get_stylesheet_directory_uri() . '/js/jquery-animate-css-rotate-scale.min.js', array( 'jquery', 'css-transform' ), '1.0', true);
		wp_enqueue_script( 'jquery-easing', get_stylesheet_directory_uri() . '/js/jquery-easing.js', array( 'jquery' ), '1.0', true);
		wp_enqueue_script( 'app', get_stylesheet_directory_uri() . '/js/app.js', array( 'jquery','css-transform','animate-css-rotate-scale' ), '1.0', true);
	}
	add_action('wp_enqueue_scripts', 'add_new_styles');
