<?php get_header(); ?>
	<main>
		<!--*********************************************SECTION DERNIERS JEUX*******************************************-->
					<div class="boite-fond-coul2-claire">
						<div class="boite-fixe">
							<section id="DerniersJeux" class="grille-g section">
									<h2 class="titre"><?php single_cat_title(); ?></h2>
									<?php
										$paged1 = isset($_GET['paged1']) ? (int) $_GET['paged1'] : 1;
									?>
									<?php
									$actualCatID = get_query_var('cat');
									$args = array(
										'cat' => $actualCatID,
										'posts_per_page' => 12,
										'orderby' => 'title',
										'order' => 'ASC',
										'paged' => $paged1
									);
									$query=new WP_query($args);
									 ?>
									<?php if($query->have_posts()) : ?>
										<?php while($query->have_posts()) : $query->the_post(); ?>
											<article class="m6 l4 cards cards-type-m hover-image-grow-rotate">
													<a href="<?php the_permalink(); ?>#detailsJeu">
														<figure class="height-s parent-image-grow-rotate">
															<img src="<?php the_post_thumbnail_url('full'); ?>" alt="<?php echo get_the_title(get_post_thumbnail_id()); ?>" class="image-grow-rotate">
															<figcaption class="xs-cache">
																<?php echo get_the_title(get_post_thumbnail_id()); ?>
															</figcaption>
														</figure>
													</a>
													<div class="cards-texte">
														<?php the_category(); ?>
														<a href="<?php the_permalink(); ?>#detailsJeu"><h3 class="contenu-centre"><?php the_title(); ?></h3></a>
														<div class="contenu-centre">
															<?php the_tags('<div class="post-tags"><i class="fas fa-user-tie"></i> ','','</div>'); ?>
														</div>
														<p><?php echo substr(get_the_content(), 0, strrpos(substr(get_the_content(), 0, 250), ' ')).'...' ?></p>
														<a href="<?php the_permalink() ?>#detailsJeu" class="btn btn-fond-coul1 call-to-action contenu-centre">Plus d'infos</a>
													</div>
											</article>
										<?php endwhile; ?>
									<?php else : ?>
										<p>Désolé, il n'y a rien à afficher !</p>
									<?php endif; ?>
									<?php wp_reset_postdata(); ?>
									<div class="pagination">
										<?php $paginate1_args = array(
											'format'             => '?paged1=%#%#DerniersJeux',
											'total'              => $query->max_num_pages,
											'current'            => $paged1,
											);
										?>
										<?php echo paginate_links($paginate1_args);?>
									</div>
							</section>
						</div>
					</div>
	<!--***********************************************FIN DERNIERS JEUX************************************************************-->
	<!--***********************************************SECTION TOUS LES JEUX*****************************************************-->
				<?php
					$args = array(
						'posts_per_page' => 1,
						'ignore_sticky_posts' => 1,
						'orderby' => 'rand'
					);
					$query_background=new WP_query($args);
				 ?>
				<?php if($query_background->have_posts()) : ?>
				<?php while($query_background->have_posts()) : $query_background->the_post(); ?>
				<?php
					$backgroundTousLesJeux = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
				?>
				<section id="TousLesJeux" class="section" style="background: url('<?php echo $backgroundTousLesJeux[0]; ?>') center no-repeat; background-size:cover;background-attachment: fixed;">

	          <div class="contenu-centre boite-fixe">
	            <h2 class="titre">Vous en voulez encore ?</h2>
	            <a href="<?php echo get_post_type_archive_link('post'); ?>#DerniersJeux" class="btn btn-fond-coul1 call-to-action">Voir tous les jeux</a>
	          </div>

	      </section>
				<?php endwhile; ?>
				<?php endif; ?>
				<?php wp_reset_postdata(); ?>
	<!--***********************************************FIN TOUS LES JEUX************************************************************-->
	</main>

<?php get_footer(); ?>
