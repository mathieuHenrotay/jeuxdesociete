

<!--***********************************************FOOTER************************************************************-->
		<div class="boite-fond-coul2">
			<div class="boite-fixe">
				<footer class="grille-g" id="footerPrincipal">
					<h2 class="xs-cache">jeuxdesociete.com - footer</h2>
					<article class="m4">
						<h3>Jeux de société</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, voluptate ducimus vitae autem perspiciatis molestias quod quisquam dolorem tempora consequuntur saepe, sit quibusdam, temporibus, officia!</p>
					</article>
					<div class="m4">
						<h3>Nos derniers jeux</h3>
						<ul class="archives-list">
							<?php wp_get_archives([
								'type'=>'postbypost',
								'limit'=>5
							]); ?>
						</ul>
					</div>
					<div class="m4">
						<h3>Vous cherchez quelque chose ?</h3>
						<?php get_search_form(); ?>
					</div>
					<div>
						<a href="#">
							&copy; Mathieu Henrotay, IEPS 2018
						</a>
					</div>
				</footer>
			</div>
		</div>
		<div id="fonduNoir"></div>
<!--***********************************************FIN FOOTER************************************************************-->
		<?php wp_footer(); ?>
	</body>

</html>
