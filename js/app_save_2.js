//Animations des sous-menus
var dropped=0;
jQuery(function(){
  jQuery('.dropDownMenu:not(.dropped) >a').click(function(e){
    e.preventDefault();
    jQuery(this).siblings('.sousMenu').slideDown(function(){
      jQuery(this).closest('.dropDownMenu').addClass('dropped');
    });
    dropped=1;
    jQuery('#fonduNoir').fadeIn();
  });

  jQuery(document).on('click', '.dropDownMenu.dropped >a', function(e){
    e.preventDefault();
    //si on est dans la navigation orientée tablettes et pc
    if (jQuery(this).closest('.menu-horizontal').length) {
      jQuery('#fonduNoir').fadeOut();
    }

    jQuery(this).siblings('.sousMenu').slideUp(function(){
      jQuery(this).closest('.dropDownMenu').removeClass('dropped');
      dropped=0;
    });
  });

  jQuery(document).click(function(event){
      if(!jQuery(event.target).closest('.dropDownMenu.dropped').length){
        jQuery('.dropDownMenu.dropped .sousMenu').slideUp().closest('.dropDownMenu').removeClass('dropped');
        dropped=0;
      }
  });

  jQuery(document).click(function(event){
    if(jQuery('.menu-horizontal .sousMenu:visible').length || jQuery('#navMobile:visible').length){
      if((jQuery(event.target).closest('#fonduNoir').length) || (!jQuery(event.target).closest('.menu li').length)){
        jQuery('#fonduNoir').fadeToggle();
      }
    }
  });

// script pour le menu de navigation mobile
  var vitesse=300;
  var actif=0
  function animMenu(){
    if(actif==0){
      jQuery('#hamburgerIcon').find('>div >div').last().animate({rotate:'-45deg',top:'10px',backgroundColor:'#eee'},vitesse).siblings('div').eq(0).animate({rotate:'45deg',top:'10px',backgroundColor:'#eee'},vitesse).siblings('div').eq(0).fadeOut(vitesse);
      jQuery('#navMobile').slideToggle();
      actif=1;
    }
    else{
      jQuery('#hamburgerIcon').find('>div >div').last().animate({rotate:'0',top:'20px',backgroundColor:'#73B2CF'},vitesse).siblings('div').eq(0).animate({rotate:'0',top:'0',backgroundColor:'#73B2CF'},vitesse).siblings('div').eq(0).fadeIn(vitesse);
      jQuery('#navMobile').slideToggle();
      actif=0;
    }
  }

  jQuery('#hamburgerIcon').click(function(e){
    e.preventDefault();
    animMenu();
  });
  /*Quand on dépasse les 768px, si le menu est toujours ouvert, il faut qu'il se ferme de lui-même et disparaisse*/
  function redimensionnement() {
      if(window.matchMedia("(min-width:768px)").matches) {
        if(actif==1){
          animMenu();
          jQuery('#fonduNoir').fadeToggle();
          jQuery('.dropDownMenu.dropped .sousMenu').slideUp(function(){
            jQuery(this).closest('.dropDownMenu').removeClass('dropped');
            dropped=0;
          });
        }
      }
  }
  window.addEventListener('resize', redimensionnement);
  jQuery(document).on('click', '#fonduNoir', function(e){
    if (actif==1) {
      animMenu();
    }
  });
  //au chargement de la page
  var height=jQuery(window).height();
  jQuery('#slider figure').css({height:height});
  //Quand on redimensionne
  jQuery(window).resize(function(){
    height=jQuery(window).height();
    jQuery('#slider figure').css({height:height});
  });


//Savoir si l'utilisateur est sur écran tactile

function ecran_tactile(){
	try{
		document.createEvent("TouchEvent");
		return 1;
	} catch(e){
		return 0;
	}
}
var typeEcran=parseInt(ecran_tactile());
if (typeEcran==1) {
  jQuery('.cards-type-l .cards-texte').css({bottom:'1.2rem'});
}

//Problème des ancres
/*function getUrlHash(){
	if( window.location.hash ){
		return window.location.hash.replace('#', '');
	}
	return false;
}
var posAncre = jQuery('#contact').offset().top;
if(window.location.hash){
  console.log('ancre présente');
  jQuery(window).on('scroll', function() {
    if ( jQuery(window).scrollTop() > posAncre-100 ){
      jQuery('html, body').css({scrollTop:0});
      jQuery(window).off('scroll');
    }
  });
}*/
//j'attends que le dom soit chargé
  /*jQuery(window).on('hashchange', function(e){
    //je récupère l'ancienne URL
    var oldURL=e.originalEvent.oldURL
    //je recherche le hastag eventuel
    var hashtagPos=oldURL.indexOf('#');
    if(hashtagPos!=-1){
      oldURL=oldURL.substr(hashtagPos);
      var posOld = jQuery(oldURL).offset().top;
      jQuery('html, body').scrollTop(posOld);
    }
    else{
      jQuery('html, body').scrollTop(0);
    }
    var elem=window.location.hash;
    var posAncre = jQuery(elem).offset().top;
    jQuery('html, body').animate({scrollTop: posAncre-88}, 800);
  });*/
  //si il y a un hashtag dans le lien
  if (window.location.hash) {
    var ancre = window.location.hash.substr(0, window.location.hash.lastIndexOf('-'));
    var posAncre = jQuery(ancre).offset().top;
    jQuery('html, body').animate({scrollTop: posAncre-88}, 800);
    //je capture le scroll et je remonte tout en haut
    /*jQuery(window).on('scroll', function() {

        jQuery('html, body').scrollTop(0);
        //j'annule l'écoute sur le scroll
        jQuery(window).off('scroll');
        //je récupère l'id vers lequel l'ancre va
        var elem=window.location.hash;
        var posAncre = jQuery(elem).offset().top;
        jQuery('html, body').animate({scrollTop: posAncre-88}, 800);
        //
    });*/
  }
  /*jQuery('#test').click(function(e){
    e.preventDefault();
    jQuery('html, body').scrollTop(0);
  });*/

  jQuery("a[href*='#']").click(function(e){
    e.preventDefault();
    var oldLink=window.location.href;
    if (oldLink.indexOf('#')>0) {
      oldLink=oldLink.substr(0,oldLink.indexOf('#'));
    }
    console.log(oldLink);

    var link=jQuery(this).attr('href');
    var ancre=link.substr(link.indexOf('#'));
    var linkSansAncre=link.substr(0,link.indexOf('#'))
    console.log(linkSansAncre);
    if (oldLink.trim()==linkSansAncre.trim()) {
      var posAncre = jQuery(ancre).offset().top;
      jQuery('html, body').animate({scrollTop: posAncre-88}, 800);
      if (jQuery('.menu horizontal .dropped')) {
        jQuery('#fonduNoir').click();
      }
    }
    else{
      window.location.href = link+'-ancre';
    }
  });


/*jQuery(window).load(function(){
  if(jQuery('.ancre')){
    var posAncre = Math.floor(jQuery('#contact').offset().top);
    console.log(posAncre);
    jQuery('html, body').animate({scrollTop: posAncre-80}, 'slow');
  }
});*/


/*var posAncre = jQuery('#contact').offset().top;

jQuery(window).on('scroll', function() {
  if ( jQuery(window).scrollTop() > posAncre-100 ){
    jQuery(window).scrollTop(posAncre-110);
    jQuery(window).off('scroll');
  }
});*/


});
