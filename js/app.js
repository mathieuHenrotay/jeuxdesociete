//Animations des sous-menus
var dropped=0;
jQuery(function(){
  jQuery('.dropDownMenu:not(.dropped) >a').click(function(e){
    e.preventDefault();
    jQuery(this).siblings('.sousMenu').slideDown(function(){
      jQuery(this).closest('.dropDownMenu').addClass('dropped');
    });
    dropped=1;
    jQuery('#fonduNoir').fadeIn();
  });

  jQuery(document).on('click', '.dropDownMenu.dropped >a', function(e){
    e.preventDefault();
    //si on est dans la navigation orientée tablettes et pc
    if (jQuery(this).closest('.menu-horizontal').length) {
      jQuery('#fonduNoir').fadeOut();
    }

    jQuery(this).siblings('.sousMenu').slideUp(function(){
      jQuery(this).closest('.dropDownMenu').removeClass('dropped');
      dropped=0;
    });
  });

  jQuery(document).click(function(event){
      if(!jQuery(event.target).closest('.dropDownMenu.dropped').length){
        jQuery('.dropDownMenu.dropped .sousMenu').slideUp().closest('.dropDownMenu').removeClass('dropped');
        dropped=0;
      }
  });

  jQuery(document).click(function(event){
    if(jQuery('.menu-horizontal .sousMenu:visible').length || jQuery('#navMobile:visible').length){
      if((jQuery(event.target).closest('#fonduNoir').length) || (!jQuery(event.target).closest('.menu li').length)){
        jQuery('#fonduNoir').fadeToggle();
      }
    }
  });

// script pour le menu de navigation mobile
  var vitesse=300;
  var actif=0
  function animMenu(){
    if(actif==0){
      jQuery('#hamburgerIcon').find('>div >div').last().animate({rotate:'-45deg',top:'10px',backgroundColor:'#eee'},vitesse).siblings('div').eq(0).animate({rotate:'45deg',top:'10px',backgroundColor:'#eee'},vitesse).siblings('div').eq(0).fadeOut(vitesse);
      jQuery('#navMobile').slideToggle();
      actif=1;
    }
    else{
      jQuery('#hamburgerIcon').find('>div >div').last().animate({rotate:'0',top:'20px',backgroundColor:'#73B2CF'},vitesse).siblings('div').eq(0).animate({rotate:'0',top:'0',backgroundColor:'#73B2CF'},vitesse).siblings('div').eq(0).fadeIn(vitesse);
      jQuery('#navMobile').slideToggle();
      actif=0;
    }
  }

  jQuery('#hamburgerIcon').click(function(e){
    e.preventDefault();
    animMenu();
  });
  /*Quand on dépasse les 768px, si le menu est toujours ouvert, il faut qu'il se ferme de lui-même et disparaisse*/
  function redimensionnement() {
      if(window.matchMedia("(min-width:768px)").matches) {
        if(actif==1){
          animMenu();
          jQuery('#fonduNoir').fadeToggle();
          jQuery('.dropDownMenu.dropped .sousMenu').slideUp(function(){
            jQuery(this).closest('.dropDownMenu').removeClass('dropped');
            dropped=0;
          });
        }
      }
  }
  window.addEventListener('resize', redimensionnement);
  jQuery(document).on('click', '#fonduNoir', function(e){
    if (actif==1) {
      animMenu();
    }
  });
  //au chargement de la page, on adapte la hauteur du slider
  /*var height=jQuery(window).height();
  jQuery('#slider figure').css({height:height});
  //Quand on redimensionne
  jQuery(window).resize(function(){
    height=jQuery(window).height();
    jQuery('#slider figure').css({height:height});
  });*/


//Savoir si l'utilisateur est sur écran tactile

function ecran_tactile(){
	try{
		document.createEvent("TouchEvent");
		return 1;
	} catch(e){
		return 0;
	}
}
var typeEcran=parseInt(ecran_tactile());
if (typeEcran==1) {
  jQuery('.cards-type-l .cards-texte').css({bottom:'1.2rem'});
}

//Problème des ancres
  //quand je clique sur un line qui possède une ancre
  jQuery('body').on('click', "a[href*='#']", function(e){
    e.preventDefault();
    var oldLink=window.location.href;
    if (oldLink.indexOf('#')>0) {
      oldLink=oldLink.substr(0,oldLink.indexOf('#'));
    }
    console.log(oldLink);

    var link=jQuery(this).attr('href');
    var ancre=link.substr(link.indexOf('#'));
    var linkSansAncre=link.substr(0,link.indexOf('#'))
    console.log(linkSansAncre);
    if (oldLink.trim()==linkSansAncre.trim()) {
      var posAncre = jQuery(ancre).offset().top;
      jQuery('html, body').animate({scrollTop: posAncre-80}, 800);
      if (jQuery('.menu horizontal .dropped')) {
        jQuery('#fonduNoir').click();
      }
    }
    else{
      window.location.href = link+'-ancre';
    }
  });

  //si il y a une ancre dans le lien
  if (window.location.hash) {
    var ancre = window.location.hash.substr(0, window.location.hash.lastIndexOf('-'));
    var posAncre = jQuery(ancre).offset().top;
    jQuery('html, body').animate({scrollTop: posAncre-80}, 800);
  }


  //Rajouter les ancres là où c'est nécessaire
  jQuery('.post-categories a, .post-tags a').each(function(){
    var link=jQuery(this).attr('href');
    jQuery(this).attr('href', link+'#DerniersJeux');
  });

  jQuery('.archives-list a').each(function(){
    var link=jQuery(this).attr('href');
    jQuery(this).attr('href', link+'#detailsJeu');
  });

  //gérer les apparitions
  var duration_default=1000;
  var delay_default=300;
  var easing_default='easeOutCubic';
  var anim_type_default='top'
  jQuery(document).scroll(function(e){
    var position=jQuery(window).scrollTop();
    console.log(position);
    jQuery('#slider img').css({'object-position':'50% calc(-'+position*0.6+'px + 50%)'})

    jQuery('.effet-apparition:not(.done)').each(function(){
      if (jQuery(this).offset().top+(jQuery(this).height()/2)<position+jQuery(window).height()) {
        jQuery(this).addClass('done');
        if (jQuery(this).attr('data-duration')) {
          duration=parseInt(jQuery(this).attr('data-duration'));
        }
        else{
          duration=duration_default;
        }
        if (jQuery(this).attr('data-delay')) {
          delay=parseInt(jQuery(this).attr('data-delay'));
        }
        else{
          delay=delay_default;
        }
        if (jQuery(this).attr('data-easing')) {
          easing=jQuery(this).attr('data-easing');
        }
        else{
          easing=easing_default;
        }
        if (jQuery(this).hasClass('apparition-scale')) {
          jQuery(this).css({'transform':'scale(0)'});
          setTimeout(()=>{
            jQuery(this).css({'transition':'transform .75s ease-out','transform':'scale(1)'});
          }, delay);
        }
        jQuery(this).delay(delay).animate({'top':0,'left':0,'opacity':1},duration,easing);
      }
    });
    /*jQuery(this).animate({'top':0,'opacity':1},600,'easeInOutSine');
    console.log(jQuery(this).offset().top);
    console.log(jQuery(window).scrollTop()+100);*/
  });

  //ajout de classes
  jQuery('#coupDeCoeur article:nth-of-type(2n)').addClass('apparition-right');
  jQuery('#coupDeCoeur article:nth-of-type(2n+1)').addClass('apparition-left');

});
