//Animations des sous-menus
var dropped=0;
jQuery(function(){
  jQuery('.dropDownMenu:not(.dropped) >a').click(function(e){
    e.preventDefault();
    jQuery(this).siblings('.sousMenu').slideDown(function(){
      jQuery(this).closest('.dropDownMenu').addClass('dropped');
    });
    dropped=1;
    jQuery('#fonduNoir').fadeIn();
  });

  jQuery(document).on('click', '.dropDownMenu.dropped >a', function(e){
    e.preventDefault();
    jQuery('#fonduNoir').fadeOut();
    jQuery(this).siblings('.sousMenu').slideUp(function(){
      jQuery(this).closest('.dropDownMenu').removeClass('dropped');
      dropped=0;
    });
  });

  jQuery(document).click(function(event){
      if(!jQuery(event.target).closest('.dropDownMenu.dropped').length){
        jQuery('.dropDownMenu.dropped .sousMenu').slideUp().closest('.dropDownMenu').removeClass('dropped');
        dropped=0;
      }
  });

  jQuery(document).click(function(event){
      if((jQuery(event.target).closest('#fonduNoir').length) || (!jQuery(event.target).closest('.menu li').length)){
        jQuery('#fonduNoir').fadeToggle();
      }
  });

// script pour le menu de navigation mobile
  var vitesse=300;
  var actif=0
  function animMenu(){
    if(actif==0){
      jQuery('#hamburgerIcon').find('>div >div').last().animate({rotate:'-45deg',top:'10px',backgroundColor:'#fff'},vitesse).siblings('div').eq(0).animate({rotate:'45deg',top:'10px',backgroundColor:'#fff'},vitesse).siblings('div').eq(0).fadeOut(vitesse);
      jQuery('#navMobile').slideToggle();
      actif=1;
    }
    else{
      jQuery('#hamburgerIcon').find('>div >div').last().animate({rotate:'0',top:'20px',backgroundColor:'#73B2CF'},vitesse).siblings('div').eq(0).animate({rotate:'0',top:'0',backgroundColor:'#73B2CF'},vitesse).siblings('div').eq(0).fadeIn(vitesse);
      jQuery('#navMobile').slideToggle();
      actif=0;
    }
  }

  jQuery('#hamburgerIcon').click(function(e){
    e.preventDefault();
    animMenu();
  });
  /*Quand on dépasse les 768px, si le menu est toujours ouvert, il faut qu'il se ferme de lui-même et disparaisse*/
  function redimensionnement() {
      if(window.matchMedia("(min-width:768px)").matches) {
        if(actif==1){
          animMenu();
        }
      }
  }
window.addEventListener('resize', redimensionnement);
});
